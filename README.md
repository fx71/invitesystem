# Scala + Play framework REST API demo app 
### Author: Michal Zyzek

Backend of REST API for example invitation system. 


##How to run
### #1 Use prebuild zip
This is the simplest way of running application. Just:
* Download and unzip file demobuild/invitesystem-1.0.zip. Try to unzip to folder with short absolute path e.g `C:/invite`.
Long paths could cause problems with some OSs.
* Run invitesystem-1.0\bin\invitesystem.bat (for Windows)
* Application server starts. You should see output like:
```
[info] play.api.Play - Application started (Prod)
[info] p.c.s.AkkaHttpServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000
```
* You are ready to test application REST API. 

### #2 Build and run app using SBT
E.g.:
* `sbt compile`
* `sbt run`

Note: This requires to prepare your development environment before(out of scope for this doc).  

##How to use
This app has several REST API endpoints. You can test it using e.g. tools like Postman 
or connect it to another app using REST. Following requests are supported:

####LIST INVITATIONS
 Returns JSON with all invitations existing in database. 
 
 Example call:

```
GET: localhost:9000/invitation
```
 
 Example result:
 
 ```
 [
       {
           "invitee": "Adam Sandler",
           "email": "adam@sand.mx",
           "status": "Pending"
       },
       {
           "invitee": "John Smith ",
           "email": "john@smith.mx",
           "status": "Pending"
       }
   ]
   ```
 

####CREATE INVITATION
Creates new invitation.

Example call:
 
 ```
 POST: localhost:9000/invitation
 Content-Type: application/json;charset=utf-8
 Body: {
       	"invitee": "Adam Sandler",
       	"email": "adam@sand.mx"
       }
 ```

 If invitation does not appear in DB then return result:
 ```Created invitation for :Adam Sandler```
 Invitation persisted and email sent to invitee
 
 If invitation exists in DB then return result:
 ```Invitation for email: adam@sand.mx already exists. Can't create new invitation for the same email.```
 No other actions in database and emailService.

####CONFIRM INVITATION

Confirms existing invitation.
 
 Example call:
 
 ```
 PUT: localhost:9000/confirm/john@smith.mx
 ```
 where `john@smith.mx` is email of specific invitation.
 
 If invitation exists in DB return message: `Invitation for adam@sand.mx was confirmed.` Invitation for this email change its status
 to `Confirmed` and emailService sends message to console output.
 
 If invitation doesn't exists in DB return message like: `Invitation another@person.mx was not found. Can't confirm this invitation.`  
 
####DECLINE INVITATION
Declines existing invitation

Example call:
 
 ```
 PUT: localhost:9000/decline/adam@sand.mx
 ```
 
  If invitation exists in DB return message: `Invitation for adam@sand.mx was declined.` Invitation for this email 
  change its status to `Declined` and emailService sends message to console output.
  
   If invitation doesn't exists in DB return message like: `Invitation another@person.mx was not found. Can't decline this invitation.`  
   