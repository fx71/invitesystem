package repository

import json.Invitation
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class InvitationRepositoryTest extends PlaySpec with MockFactory with BeforeAndAfter {

  var repository: InvitationRepository = _

  before {
    repository = new InvitationRepositoryInMemory
  }

  "InvitationRepository#getInvitations " should {
    "return empty list when repository just initialized" in {
      //When
      val resultInvitations: List[Invitation] = repository.getInvitations
      //Then
      resultInvitations isEmpty
    }
  }

  "InvitationRepository " should {
    "return one invitation after it was added" in {
      //Given
      val expectedInvitation = Invitation("name", "email")
      //When
      repository.upSertInvitation(expectedInvitation)
      val resultInvitations: List[Invitation] = repository.getInvitations
      //Then
      resultInvitations.size mustBe 1
      resultInvitations(0) mustBe expectedInvitation
    }
  }

  "InvitationRepository " should {
    "update existing invitation" in {
      //Given
      val firstInvitation = Invitation("name before update", "email")
      val updatedInvitation = Invitation("name after update", "email")
      //When
      repository.upSertInvitation(firstInvitation)
      repository.upSertInvitation(updatedInvitation)
      val resultInvitations: List[Invitation] = repository.getInvitations
      //Then
      resultInvitations.size mustBe 1
      resultInvitations(0) mustBe updatedInvitation
    }
  }

  "InvitationRepository#containsInvitation " should {
    "returns true when invitation exists in DB" in {
      //Given
      val expectedInvitation = Invitation("name before update", "email")
      //When
      repository.upSertInvitation(expectedInvitation)
      val existsResult: Boolean = repository.containsInvitation(expectedInvitation.email)
      //Then
      existsResult mustBe true
    }
  }

  "InvitationRepository#containsInvitation " should {
    "returns false when invitation not exists in DB" in {
      //Given
      val existingInvitation = Invitation("first name", "email one")
      val anotherInvitations = Invitation("second name", "email two")
      //When
      repository.upSertInvitation(existingInvitation)
      val existsResult: Boolean = repository.containsInvitation(anotherInvitations.email)
      //Then
      existsResult mustBe false
    }
  }

  "InvitationRepository#getInvitation " should {
    "returns None when invitation not found" in {
      //Given
      val invitation = Invitation("first name", "email one")
      //When
      val result: Option[Invitation] = repository.getInvitation(invitation.email)
      //Then
      result mustBe Option.empty
    }
  }

  "InvitationRepository#getInvitation " should {
    "returns proper invitation when exists in DB" in {
      //Given
      val expectedInvitation = Invitation("first name", "email one")
      //When
      repository.upSertInvitation(expectedInvitation)
      val result: Option[Invitation] = repository.getInvitation(expectedInvitation.email)
      //Then
      result.isEmpty mustBe false
      result.get mustBe expectedInvitation
    }
  }

}
