package json

import org.scalamock.scalatest.MockFactory
import org.scalatestplus.play.PlaySpec

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class InvitationTest extends PlaySpec with MockFactory {

  "Invitation model" should {
    "set fields properly" in {
      //Given
      val expectedInvitee = "John Smith"
      val expectedEmail = "jsmith@google.com"
      //When
      val resultInvitation: Invitation = Invitation(expectedInvitee, expectedEmail)
      //Then
      resultInvitation.email mustBe expectedEmail
      resultInvitation.invitee mustBe expectedInvitee
    }
  }

  "Invitation model" should {
    "has initial Pending status" in {
      //Given
      val expectedInvitee = "John Smith"
      val expectedEmail = "jsmith@google.com"
      //When
      val resultInvitation: Invitation = Invitation(expectedInvitee, expectedEmail)
      //Then
      resultInvitation.status mustBe InviteStatus.Pending
    }
  }

  "Invitation model" should {
    "keep non default invitation status " in {
      //Given
      val expectedInvitee = "John Smith"
      val expectedEmail = "jsmith@google.com"
      val expectedStatus = InviteStatus.Confirmed
      //When
      val resultInvitation: Invitation = Invitation(expectedInvitee, expectedEmail, expectedStatus)
      //Then
      resultInvitation.status mustBe expectedStatus
    }
  }

}
