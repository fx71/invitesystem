package controllers

import json.Invitation
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play._
import play.api.libs.json.Json
import play.api.mvc._
import play.api.test.Helpers._
import play.api.test._
import services.InviteService

import scala.concurrent.Future

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class InviteControllerTest extends PlaySpec with Results with MockFactory with BeforeAndAfter {

  var controllerComponents: ControllerComponents = _
  var inviteService: InviteService = _
  var controller: InviteController = _

  before {
    controllerComponents = stubControllerComponents()
    inviteService = stub[InviteService]
    controller = new InviteController(controllerComponents, inviteService)
  }

  "InviteController#listInvitations GET" should {
    "return empty list when no invitations in DB" in {
      //Given
      (inviteService.getAllInvitations _).when().returns(List())
      //When
      val result: Future[Result] = controller.listInvitations().apply(FakeRequest())
      //Then
      val invitationResult: List[Invitation] = contentAsJson(result).as[List[Invitation]]
      invitationResult mustBe empty
    }
  }

  "InviteController#listInvitations GET" should {
    "return 2 invitations from DB" in {
      //Given
      (inviteService.getAllInvitations _).when().returns(List(Invitation("name 1", "email 1"), Invitation("name 2", "email 2")))
      //When
      val result: Future[Result] = controller.listInvitations().apply(FakeRequest())
      //Then
      val invitationResult: List[Invitation] = contentAsJson(result).as[List[Invitation]]
      invitationResult.size mustBe 2
      invitationResult(0).invitee mustBe "name 1"
      invitationResult(0).email mustBe "email 1"
      invitationResult(1).invitee mustBe "name 2"
      invitationResult(1).email mustBe "email 2"
    }
  }

  "InviteController#listInvitations " should {
    "return Ok type when success" in {
      //Given
      (inviteService.getAllInvitations _).when().returns(List())
      //When
      val result: Future[Result] = controller.listInvitations().apply(FakeRequest())
      //Then
      status(result) mustBe OK
    }
  }

  "InviteController#createInvitation POST" should {
    "return proper val" in {
      //Given
      val fakeRequest = FakeRequest(POST, "/invitation", FakeHeaders(), AnyContentAsJson(Json.parse("""{ "invitee": "John Smith", "email": "john@smith.mx" }""")))
      //When
      val result: Future[Result] = controller.createInvitation().apply(fakeRequest)
      //Then
      val bodyText: String = contentAsString(result)
      bodyText mustBe "Created invitation for :John Smith"
    }
  }

  "InviteController#createInvitation" should {
    "return Created type when success" in {
      //Given
      val fakeRequest = FakeRequest(POST, "/invitation", FakeHeaders(), AnyContentAsJson(Json.parse("""{ "invitee": "John Smith", "email": "john@smith.mx" }""")))
      //When
      val result: Future[Result] = controller.createInvitation().apply(fakeRequest)
      //Then
      status(result) mustBe CREATED
    }
  }

  "InviteController#createInvitation" should {
    "return Conflict type when invitation already exixts" in {
      //Given
      val fakeRequest = FakeRequest(POST, "/invitation", FakeHeaders(), AnyContentAsJson(Json.parse("""{ "invitee": "John Smith", "email": "john@smith.mx" }""")))
      (inviteService.containsInvitation _).when("john@smith.mx").returns(true)
      //When
      val result: Future[Result] = controller.createInvitation().apply(fakeRequest)
      //Then
      status(result) mustBe CONFLICT
    }
  }

  "InviteController#createInvitation" should {
    "call service properly" in {
      //Given
      val expectedName = "John Smith"
      val expectedEmail = "john@smith.mx"
      val fakeRequest = FakeRequest(POST, "/invitation", FakeHeaders(), AnyContentAsJson(Json.parse(s"""{ "invitee": "$expectedName", "email": "$expectedEmail" }""")))
      //When
      val result: Future[Result] = controller.createInvitation().apply(fakeRequest)
      //Then
      (inviteService.addInvitation _).verify(Invitation(expectedName, expectedEmail)).once()
    }
  }

  "InviteController#confirm" should {
    "return OK status when invitation exists in DB" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(true)
      val fakeRequest = FakeRequest(PUT, "/confirm")
      //When
      val result: Future[Result] = controller.confirm(expectedEmail).apply(fakeRequest)
      //Then
      status(result) mustBe OK
    }
  }

  "InviteController#confirm" should {
    "return NotFound status when invitation not exists in DB" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(false)
      val fakeRequest = FakeRequest(PUT, "/confirm")
      //When
      val result: Future[Result] = controller.confirm(expectedEmail).apply(fakeRequest)
      //Then
      status(result) mustBe NOT_FOUND
    }
  }

  "InviteController#confirm" should {
    "call service.confirm with email argument" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(true)
      val fakeRequest = FakeRequest(PUT, "/confirm")
      //When
      controller.confirm(expectedEmail).apply(fakeRequest)
      //Then
      (inviteService.confirm _).verify(expectedEmail).once()
    }
  }

  "InviteController#decline" should {
    "return OK status when invitation exists in DB" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(true)
      val fakeRequest = FakeRequest(PUT, "/decline")
      //When
      val result: Future[Result] = controller.decline(expectedEmail).apply(fakeRequest)
      //Then
      status(result) mustBe OK
    }
  }

  "InviteController#decline" should {
    "return NotFound status when invitation not exists in DB" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(false)
      val fakeRequest = FakeRequest(PUT, "/decline")
      //When
      val result: Future[Result] = controller.decline(expectedEmail).apply(fakeRequest)
      //Then
      status(result) mustBe NOT_FOUND
    }
  }

  "InviteController#decline" should {
    "call service.decline with email argument" in {
      //Given
      val expectedEmail = "john@smith.mx"
      (inviteService.containsInvitation _).when(expectedEmail).returns(true)
      val fakeRequest = FakeRequest(PUT, "/decline")
      //When
      controller.decline(expectedEmail).apply(fakeRequest)
      //Then
      (inviteService.decline _).verify(expectedEmail).once()
    }
  }

}