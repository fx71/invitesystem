package services

import json.{Invitation, InviteStatus}
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec
import repository.InvitationRepository

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class InviteServiceTest extends PlaySpec with MockFactory with BeforeAndAfter {

  var inviteRepository: InvitationRepository = _
  var emailService: EmailService = _
  var inviteService: InviteService = _

  before {
    inviteRepository = stub[InvitationRepository]
    emailService = stub[EmailService]
    inviteService = new AtomicInviteService(inviteRepository, emailService)
  }

  "InviteService#getAllInvitations" should {
    "call repository getInvitations" in {
      //When
      inviteService.getAllInvitations
      //Then
      (inviteRepository.getInvitations _).verify().once()
    }
  }

  "InviteService#addInvitation" should {
    "call repository upSertInvitation" in {
      //Given
      val expectedInvitation: Invitation = Invitation("any name", "any email")
      //When
      inviteService.addInvitation(expectedInvitation)
      //Then
      (inviteRepository.upSertInvitation _).verify(expectedInvitation).once()
    }
  }

  "InviteService#addInvitation" should {
    "call emailService#sendInvitation" in {
      //Given
      val expectedInvitation: Invitation = Invitation("any name", "any email")
      //When
      inviteService.addInvitation(expectedInvitation)
      //Then
      (emailService.sendInvitation _).verify(expectedInvitation.email).once()
    }
  }

  "InviteService#confirm" should {
    "upsert confirmed invitation" in {
      //Given
      val invitationBeforeConfirm = Invitation("inv name", "email")
      val invitationAfterConfirm = Invitation(invitationBeforeConfirm.invitee, invitationBeforeConfirm.email, InviteStatus.Confirmed)
      (inviteRepository.getInvitation _).when(invitationBeforeConfirm.email).returns(Option.apply(invitationBeforeConfirm))
      //When
      inviteService.confirm(invitationBeforeConfirm.email)
      //Then
      (inviteRepository.upSertInvitation _).verify(invitationAfterConfirm).once()
    }
  }

  "InviteService#confirm" should {
    "call emailService#sendConfirmation" in {
      //Given
      val invitationBeforeConfirm = Invitation("inv name", "email")
      (inviteRepository.getInvitation _).when(invitationBeforeConfirm.email).returns(Option.apply(invitationBeforeConfirm))
      //When
      inviteService.confirm(invitationBeforeConfirm.email)
      //Then
      (emailService.sendConfirmation _).verify(invitationBeforeConfirm.email).once()
    }
  }

  "InviteService#decline" should {
    "upsert declined invitation" in {
      //Given
      val invitationBeforeDecline = Invitation("inv name", "email")
      val invitationAfterDecline = Invitation(invitationBeforeDecline.invitee, invitationBeforeDecline.email, InviteStatus.Declined)
      (inviteRepository.getInvitation _).when(invitationBeforeDecline.email).returns(Option.apply(invitationBeforeDecline))
      //When
      inviteService.decline(invitationBeforeDecline.email)
      //Then
      (inviteRepository.upSertInvitation _).verify(invitationAfterDecline).once()
    }
  }

  "InviteService#decline" should {
    "call emailService#sendDecline" in {
      //Given
      val invitationBeforeConfirm = Invitation("inv name", "email")
      (inviteRepository.getInvitation _).when(invitationBeforeConfirm.email).returns(Option.apply(invitationBeforeConfirm))
      //When
      inviteService.decline(invitationBeforeConfirm.email)
      //Then
      (emailService.sendDecline _).verify(invitationBeforeConfirm.email).once()
    }
  }

  "InviteService#containsInvitation" should {
    "call repository containsInvitation" in {
      //Given
      val expectedEmail: String = "exampleEmail"
      //When
      inviteService.containsInvitation(expectedEmail)
      //Then
      (inviteRepository.containsInvitation _).verify(expectedEmail).once()
    }
  }

}
