package services

import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfter
import org.scalatestplus.play.PlaySpec

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class EmailServiceTest extends PlaySpec with MockFactory with BeforeAndAfter {

  var mailService: EmailService = _
  val expectedEmail: String = "expectedEmial@com"

  before {
    mailService = new EmailServiceMock()
  }

  "EmailService#sendInvitation" should {
    "print messege with expectedEmail and Confirm type" in {
      //Given
      val expectedType: String = "Invite"
      //When
      val result: String = mailService.sendInvitation(expectedEmail)
      //Then
      result.contains(s"MailTo:$expectedEmail") mustBe true
      result.contains(s"Subject:$expectedType") mustBe true
    }
  }

  "EmailService#sendConfirmation" should {
    "print messege with expectedEmail and Confirm type" in {
      //Given
      val expectedType: String = "Confirm"
      //When
      val result: String = mailService.sendConfirmation(expectedEmail)
      //Then
      result.contains(s"MailTo:$expectedEmail") mustBe true
      result.contains(s"Subject:$expectedType") mustBe true
    }
  }

  "EmailService#sendDecline" should {
    "print messege with expectedEmail and Confirm type" in {
      //Given
      val expectedType: String = "Decline"
      //When
      val result: String = mailService.sendDecline(expectedEmail)
      //Then
      result.contains(s"MailTo:$expectedEmail") mustBe true
      result.contains(s"Subject:$expectedType") mustBe true
    }
  }

}
