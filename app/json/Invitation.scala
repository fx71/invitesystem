package json

import json.InviteStatus.InviteStatus
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
  * @param invitee name of invitee
  * @param email   invitee email. Primary key of invitation object
  * @param status  identifies current status of invitation. Default status is Pending
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
case class Invitation(invitee: String, email: String, status: InviteStatus = InviteStatus.Pending) {
}

object Invitation {

  val invitationReads: Reads[Invitation] = (
    (JsPath \ "invitee").read[String] and
      (JsPath \ "email").read[String] and
      ((JsPath \ "status").read[InviteStatus] or Reads.pure(InviteStatus.Pending))
    ) (Invitation.apply _)

  val invitationWrites: Writes[Invitation] = Json.writes[Invitation]

  implicit val invitationFormat: Format[Invitation] =
    Format(invitationReads, invitationWrites)

}
