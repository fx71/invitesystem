package json

import play.api.libs.json.Reads

/**
  * Enum which defines possible statuses of invitation.
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
object InviteStatus extends Enumeration {
  type InviteStatus = Value
  val Pending, Confirmed, Declined = Value
  implicit val statusReads = Reads.enumNameReads(InviteStatus)
}
