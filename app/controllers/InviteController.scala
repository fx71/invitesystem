package controllers

import javax.inject._
import json.Invitation
import play.api.libs.json.Json
import play.api.mvc._
import services.InviteService

/**
  * This controller is responsible for managing invitation in system.
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
@Singleton
class InviteController @Inject()(cc: ControllerComponents,
                                 inviteService: InviteService) extends AbstractController(cc) {

  /**
    * Lists all invitations
    */
  def listInvitations = Action {
    val allInvitations: List[Invitation] = inviteService.getAllInvitations
    val resultJson = Json.toJson(allInvitations)
    Ok(resultJson)
  }

  /**
    * Adds new invitation
    */
  def createInvitation = Action { request =>
    val requestJson = request.body.asJson.get
    val newInvitation: Invitation = requestJson.as[Invitation]
    if (inviteService.containsInvitation(newInvitation.email)) {
      Conflict(s"Invitation for email: ${newInvitation.email} already exists. Can't create new invitation for the same email.")
    }
    else {
      inviteService.addInvitation(newInvitation)
      Created("Created invitation for :" + newInvitation.invitee)
    }

  }

  def confirm(email: String) = Action { request =>
    if (!inviteService.containsInvitation(email)) {
      NotFound(s"Invitation $email was not found. Can't confirm this invitation.")
    }
    else {
      inviteService.confirm(email)
      Ok(s"Invitation for ${email} was confirmed.")
    }
  }

  def decline(email: String) = Action { request =>
    if (!inviteService.containsInvitation(email)) {
      NotFound(s"Invitation $email was not found. Can't decline this invitation.")
    }
    else {
      inviteService.decline(email)
      Ok(s"Invitation for ${email} was declined.")
    }
  }

}
