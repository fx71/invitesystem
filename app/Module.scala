import java.time.Clock

import com.google.inject.AbstractModule
import repository.{InvitationRepository, InvitationRepositoryInMemory}
import services.{AtomicInviteService, EmailService, EmailServiceMock, InviteService}

/**
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
class Module extends AbstractModule {

  override def configure() = {
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone)

    bind(classOf[InviteService]).to(classOf[AtomicInviteService])

    bind(classOf[InvitationRepository]).to(classOf[InvitationRepositoryInMemory])

    bind(classOf[EmailService]).to(classOf[EmailServiceMock])
  }

}
