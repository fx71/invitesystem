package services

import javax.inject._
import play.api.Logger

/**
  * Mock for email service. In full application this could be an interface for real email service. This version only prints
  * output necessary to verify if email was triggered properly.
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
trait EmailService {

  def sendInvitation(email: String): String

  def sendConfirmation(email: String): String

  def sendDecline(email: String): String

}

@Singleton
class EmailServiceMock @Inject()() extends EmailService {

  protected def sendEmail(emailTo: String, mailType: String): String = {
    val message: String = s"Mock email service: MailTo:$emailTo Subject:$mailType"
    Logger.info(message)
    message
  }

  override def sendInvitation(emailTo: String): String = {
    sendEmail(emailTo, "Invite")
  }

  override def sendConfirmation(emailTo: String): String = {
    sendEmail(emailTo, "Confirm")
  }

  override def sendDecline(emailTo: String): String = {
    sendEmail(emailTo, "Decline")
  }
}
