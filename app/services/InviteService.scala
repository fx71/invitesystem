package services

import javax.inject._
import json.InviteStatus.InviteStatus
import json.{Invitation, InviteStatus}
import repository.InvitationRepository

/**
  * InviteService orchestrates repository and emailService properly. Responsible for calling persistence services and
  * and email service.
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
trait InviteService {
  def getAllInvitations: List[Invitation]

  def addInvitation(newInvitation: Invitation)

  def confirm(email: String)

  def decline(email: String)

  def containsInvitation(email: String): Boolean
}

@Singleton
class AtomicInviteService @Inject()(inviteRepository: InvitationRepository, emailService: EmailService) extends InviteService {
  override def getAllInvitations: List[Invitation] = inviteRepository.getInvitations

  override def addInvitation(newInvitation: Invitation): Unit = {
    inviteRepository.upSertInvitation(newInvitation)
    emailService.sendInvitation(newInvitation.email)
  }

  private def updateStatus(email: String, newStatus: InviteStatus): Unit = {
    inviteRepository.getInvitation(email).foreach(inviteBeforeUpdate => {
      val inviteAfterUpdate = Invitation(inviteBeforeUpdate.invitee, inviteBeforeUpdate.email, newStatus)
      inviteRepository.upSertInvitation(inviteAfterUpdate)
    })
  }

  override def confirm(email: String): Unit = {
    updateStatus(email, InviteStatus.Confirmed)
    emailService.sendConfirmation(email)
  }

  override def decline(email: String): Unit = {
    updateStatus(email, InviteStatus.Declined)
    emailService.sendDecline(email)
  }

  override def containsInvitation(email: String): Boolean = inviteRepository.containsInvitation(email)

}
