package repository

import java.util.concurrent.ConcurrentHashMap

import javax.inject.Singleton
import json.Invitation

import scala.collection.JavaConverters._
import scala.collection.concurrent

/**
  * Invitations repository. Supports limited set of functions(minimum for this demo).
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
trait InvitationRepository {
  /**
    *
    * @return List of all invitations
    */
  def getInvitations: List[Invitation]

  /**
    * Upsert uses concept from MongoDB. It adds new record of updates existing one(in case the key is present)
    *
    * @param invitation new or updated invitation
    */
  def upSertInvitation(invitation: Invitation)

  /**
    * Checks if invitation with following email exists.
    *
    * @param email of invitation
    */
  def containsInvitation(email: String): Boolean

  /**
    *
    * @param email key
    * @return invitation found
    */
  def getInvitation(email: String): Option[Invitation]

}

/**
  * Invitations repository.
  * Normally this would be connected to database. For demo purposes it uses inMemory Map. Email is used as key for each
  * Invitation.
  *
  * @author Michal Zyzek (zyzek.michal@gmail.com)
  */
@Singleton
class InvitationRepositoryInMemory extends InvitationRepository {

  private val invitations: concurrent.Map[String, Invitation] = new ConcurrentHashMap[String, Invitation]().asScala

  override def getInvitations: List[Invitation] = {
    invitations.values.toList
  }

  override def upSertInvitation(invitation: Invitation): Unit = {
    invitations += (invitation.email -> invitation)
  }

  /**
    * Checks if invitation with following email exists.
    *
    * @param email of invitation
    */
  override def containsInvitation(email: String): Boolean = invitations.contains(email)

  /**
    *
    * @param email key
    * @return invitation found
    */
  override def getInvitation(email: String): Option[Invitation] = invitations.get(email)
}
